# smart-list is an implementation of a linked list in NodeJS #

## Installation ##

```
#!js

npm install smart-list

```

## Example usage ##

```
#!js

var List = require("smart-list");  
var list = new List();

list.push("foo");  
console.log(list.pop());

```

## Reference ##

### List ###

- **unshift** - function (data)  
Insert an element at the beginning of the list.

- **push** - function (data)  
Insert an element at the end of the list.

- **shift** - function ()  
Retrieves the first element in the list.

- **pop** - function ()  
Retrieves the last element in the list.

- **first** - function ()  
Retrieves an Iterator to the first element.

- **last** - function ()  
Retrieves an Iterator to the last element.

- **size** or **length** - function ()  
Returns the total number of elements in the list.

### Iterator ###

- **data** - property  
The data stored that has been stored in the list. It can be also updated from here. 

- **next** - function ()  
Retrieves the next element in the list.

- **previous** - function ()  
Retrieves the previous element in the list.

- **hasNext** - function ()  
Checks if there is an element after the current one.

- **hasPrevious** - function ()  
Checks if there is an element before the current one.

- **drop** - function ()  
Removes the element from the list. Further iterations will not point to it, however the iterator itself is not immediately destroyed.

# LICENSE #
The MIT License (MIT)

Copyright (c) 2015 Atanas Plachkov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.