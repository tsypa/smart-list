"use strict";

var Iterator = require("./iterator");

var List = function () {
	this.clear();
};

List.prototype.clear = function () {
	this._length = 0;
	this._first = this._last = null;
};

List.prototype.unshift = function (data) {
	this._length++;
	var item = new Iterator(data, null, this._first, this);
	if (this._first) {
		this._first._previous = item;
	} else {
		this._last = item;
	}
	return this._first = item;
};

List.prototype.push = function (data) {
	this._length++;
	var item = new Iterator(data, this._last, null, this);
	if (this._last) {
		this._last._next = item;
	} else {
		this._first = item;
	}
	return this._last = item;
};

List.prototype.shift = function () {
	var item = this._first;
	if (!item) {
		return;
	}

	this._first.drop();
	
	return item.data;
};

List.prototype.pop = function () {
	var item = this._last;
	if (!item) {
		return;
	}

	this._last.drop();
	
	return item.data;
};

List.prototype.first = function () {
	return this._first;
};

List.prototype.last = function () {
	return this._last;
};

List.prototype.size = List.prototype.length = function () {
	return this._length;
};

module.exports = List;